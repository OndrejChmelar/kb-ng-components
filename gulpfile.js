gulp = require('gulp');
concat = require('gulp-concat');
less = require('gulp-less');

gulp.task('js', function() {
	gulp.src(['src/**/*.js'])
		.pipe(concat('kb-ng-clients-info.js'))
		.pipe(gulp.dest('dist'));
});

gulp.task('less', function() {
    gulp.src('src/**/*.less')
        .pipe(less())
        .pipe(concat('kb-ng-clients-info.css'))
        .pipe(gulp.dest('dist'))
});

gulp.task('build', ['js', 'less']);