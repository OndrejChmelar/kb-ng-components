angular.module('kb-ng-clients-info').directive('kbClientsInfo', function($window) {
    return {
        restrict: 'E',
        scope: {
            clientId: '@'
        },
        controller: function($scope, $element) {
            var index = -1;
            var children = [];
            $scope.clients = clients;

            var _nextClient = function () {
                if (++index >= clients.length) {
                    index = 0;
                }
                $scope.client = clients[index];
                children.forEach(function(scope) {
                    scope.client = $scope.client;
                });
                $window.document.title = $scope.client.name + " [KB Pageflow] 360 – ID=" + $scope.client.id;
            };
            this.addChild = function(child) {
                children.push(child);
                child.nextClient = _nextClient;
                child.client = $scope.client;
            };
            this.removeChild = function(scope) {
                var idx = children.indexOf(scope);
                if (idx > -1) {
                    children = children.splice(idx, 1);
                }
            };

            $scope.nextClient = _nextClient;
            _nextClient();
        }
    }
})

.directive('kbClientInfo', function() {
    return {
        restrict: 'E',
        require: '^^kbClientsInfo',
        templateUrl: '360/partials/client-info.html',
        link: function(scope, element, attrs, parentCt) {
            parentCt.addChild(scope);

            scope.$on('$destroy', function() {
                parentCt.removeChild(scope);
            });
        }
    };
})

.directive('kbClientLinks', function() {
    return {
        restrict: 'E',
        require: '^^kbClientsInfo',
        templateUrl: '360/partials/client-links.html',
        link: function(scope, element, attrs, parentCt) {
            parentCt.addChild(scope);
        }
    };
});
